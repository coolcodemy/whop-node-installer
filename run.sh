#!/bin/bash
touch /home/whop/panel/whop-panel/storage/logs/laravel.log
chown whop:whop /home/whop/panel/whop-panel/storage/logs/laravel.log
cd /home/whop/panel/whop-panel
yes | php /home/whop/panel/whop-panel/artisan migrate > /dev/null


# dirty work. dont judge
adminPassword=`echo "$2" | sed -e 's/\\$/\\\\\\\\$/g'`
adminPassword=`echo "$adminPassword" | sed -e 's/"/\\\\\\\\"/g'`


accountPassword=`echo "$4" | sed -e 's/\\$/\\\\$/g'`
accountPassword=`echo "$adminPassword" | sed -e 's/"/\\\\"/g'`


rm /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
cp /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php.ori /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php

cd /home/whop/panel/whop-panel
composer dump-autoload


sed -i "s/_ADMINUSERNAME_/$1/g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
sed -i "s]_ADMINPASSWORD_]$adminPassword]g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php

sed -i "s]_ACCOUNTUSERNAME_]$3]g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
sed -i "s]_ACCOUNTPASSWORD_]$accountPassword]g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
sed -i "s]_ACCOUNTNAME_]$5]g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
sed -i "s]_ACCOUNTEMAIL_]$6]g" /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php

yes | php /home/whop/panel/whop-panel/artisan db:seed > /dev/null

rm /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
composer dump-autoload


sed -i "s]LF_ALERT_TO = \"\"]LF_ALERT_TO = \"$6\"]g" /etc/csf/csf.conf

sed -i "s/_DOMAIN_/$7/g" /home/whop/panel/whop-panel/.env


whop-license "newuser" $3 $4 0 1


#update fpm for whop
echo "[whop]" > /etc/php-fpm.d/whop.conf
echo "listen = /home/whop/hosting/whop/php.socket" >> /etc/php-fpm.d/whop.conf
echo "listen.allowed_clients = 127.0.0.1" >> /etc/php-fpm.d/whop.conf
echo "listen.owner = nginx" >> /etc/php-fpm.d/whop.conf
echo "listen.group = nginx" >> /etc/php-fpm.d/whop.conf
echo "user = whop" >> /etc/php-fpm.d/whop.conf
echo "group = whop" >> /etc/php-fpm.d/whop.conf
echo "pm = ondemand" >> /etc/php-fpm.d/whop.conf
echo "pm.max_children = 20" >> /etc/php-fpm.d/whop.conf
echo "pm.process_idle_timeout = 10s" >> /etc/php-fpm.d/whop.conf
echo "pm.max_requests = 500" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[open_basedir] = /home/whop/panel/whop-panel:/home/whop/panel/session:/home/whop/panel/cache:/usr/share/php:/usr/share/pear:/etc/roundcubemail:/usr/share/roundcubemail:/var/log/roundcubemail:/usr/share/phpMyAdmin:/etc/phpMyAdmin:/var/lib/phpMyAdmin:/tmp" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[upload_tmp_dir] = \"/home/whop/panel/cache\"" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[session.save_path] = \"/home/whop/panel/session\"" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[soap.wsdl_cache_dir] = \"/home/whop/panel/cache\"" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[extension] = apcu.so" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.enabled] = 1" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.shm_segments] = 1" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.shm_size] = 64M" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.entries_hint] = 4096" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.ttl] = 7200" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.use_request_time] = 1" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.gc_ttl] = 3600" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.smart] = 0" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.mmap_file_mask] = /tmp/apc.XXXXXX" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.slam_defense] = 1" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.serializer] = 'default'" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.enable_cli] = 0" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.rfc1867] = 0" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.rfc1867_prefix] = upload" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.rfc1867_name] = APC_UPLOAD_PROGRESS" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.rfc1867_freq] = 0" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.rfc1867_ttl] = 3600" >> /etc/php-fpm.d/whop.conf
echo "php_admin_value[apc.coredump_unmap] = 0" >> /etc/php-fpm.d/whop.conf

rm -rf /etc/php-fpm.d/www.conf

/etc/init.d/php-fpm restart > /dev/null 2>&1


mv /home/whop/hosting/whop/whop-installer\|nginx.conf /home/whop/hosting/whop/whop-installer\|nginx.conf.disable
nginx -s reload

sed -i "s/_DOMAINNAME_/$7/g" /usr/share/roundcubemail/config/config.inc.php
cd ../


databaseServer=`cat /home/whop/panel/whop-panel/.env | grep "DATABASE_SERVER" | cut -f2 -d =`
emailServer=`cat /home/whop/panel/whop-panel/.env | grep "MAIL_SERVER" | cut -f2 -d =`
dnsServer=`cat /home/whop/panel/whop-panel/.env | grep "DNS_SERVER" | cut -f2 -d =`


ssh root@$emailServer "sed -i \"s/\$mydomain = 'example.com';/\$mydomain = 'mail.$7';/g\" /etc/amavisd/amavisd.conf"


# Main
echo -ne "\n127.0.0.1        $8 localhost localhost.localdomain\n" >> /etc/hosts
hostname $8
sed -i "s/HOSTNAME=.*/HOSTNAME=$8/" /etc/sysconfig/network


# Database, Email, DNS
ssh root@$databaseServer "echo -ne \"\n127.0.0.1        db.$7 localhost localhost.localdomain\n\" >> /etc/hosts"
ssh root@$emailServer "echo -ne \"\n127.0.0.1        mail.$7 localhost localhost.localdomain\n\" >> /etc/hosts"
ssh root@$dnsServer "echo -ne \"\n127.0.0.1        ns1.$7 localhost localhost.localdomain\n\" >> /etc/hosts"
ssh root@$dnsServer "echo -ne \"\n127.0.0.1        ns2.$7 localhost localhost.localdomain\n\" >> /etc/hosts"



ssh root@emailServer "service postsrsd restart"
ssh root@emailServer "service postfix reload"

supervisorctl reload
