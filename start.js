require('dotenv').config({path: '/home/whop/panel/whop-panel/.env'});

var io = require('socket.io')(30000);
var mysql = require('mysql');
var spawn = require('child_process').spawn;
var connection = Connection();
var fs = require('fs');
var exec = require('child_process').exec;
//var process = require('process');

io.on('connection', function(socket) {

	socket.on('buildServer-server', function(json)
	{
		connection.connect(function()
		{
			RegisterDomainName(io, socket, connection, json, function()
			{
                socket.emit('buildServer-client', '- Configuring DNS records');

                Exec("/usr/bin/ssh root@" + process.env.EMAIL_SERVER + " \"postconf -e 'myhostname = mail." + json.DomainName + "'\"", function() {

                    socket.emit('buildServer-client', '- Setting postfix');


                    Command('cp', ['/home/whop/panel/whop-node-installer/panel.conf', '/home/whop/hosting/whop/whop-panel|nginx.conf'], function(msg){
                        
                        fs.readFile('/home/whop/hosting/whop/whop-panel|nginx.conf', 'utf8', function (err,data) {

                            var result = data.replace(/_SERVERNAME_/g, json.ServerName);
                            fs.writeFile('/home/whop/hosting/whop/whop-panel|nginx.conf', result, 'utf8', function (err) {
                                socket.emit('buildServer-client', '- Creating panel configuration');

                                Command('/bin/bash', ['/home/whop/panel/whop-node-installer/run.sh', json.AdminUsername, json.AdminPassword, json.AccountUsername, json.AccountPassword, json.AccountName, json.PrimaryEmail, json.DomainName, json.ServerName], function(msg) {
                                    if(msg != 0) {
                                        //socket.emit('buildServer-client', msg);
                                    } else if(msg == 0) {
                                        socket.emit('buildServer-client', '- Reloading fail2ban');

                                        fs.readFile('/etc/fail2ban/jail.local', 'utf8', function (err,data) {

                                            var result = data.replace(/whop@whop.io/g, 'fail2ban@' + json.DomainName);
                                            result = result.replace(/fikri@whop.io/g, json.PrimaryEmail);

                                            fs.writeFile('/etc/fail2ban/jail.local', result, 'utf8', function (err) {
                                                Command('/etc/init.d/fail2ban', ['reload'], function(msg)
                                                {
                                                    socket.emit('buildServer-client', 'FINISH');

                                                    fs.unlink('/home/whop/panel/whop-panel-installer/public/config.json', function()
                                                    {
                                                        Command('forever', ['start', '/home/whop/panel/whop-node/start.js'], function(msg)
                                                        {
                                                            Command('forever', ['stop', '/home/whop/panel/whop-node-installer/start.js'], function(msg) {
                                                            });
                                                        });

                                                        // fix for digital ocean where they set PTR before registering the pdns
                                                        Exec('mv /etc/php.d/imap.ini.disable /etc/php.d/imap.ini; /etc/init.d/php-fpm reload', function(stdout, error, stderr)
                                                        {

                                                        });
                                                    });
                                                    
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
			});
		});
	});


    socket.on('verify-license-server', function()
    {
        Exec('/usr/bin/whop-license verifyinstall', function(stdout, error, stderr)
        {
            if (stdout == "true\n") {

            	socket.emit('verify-license-client', true);

            } else {

                socket.emit('verify-license-client', false);

            }
        });
    });


    socket.on('upload-server', function(data)
    {
        Exec('/bin/bash ' + __dirname + '/licenseupdater.sh ' + data['licensePath'], function(stdout, error, stderr)
        {
        });
    });

});



function Command(command, arg, callback) 
{
    var sp = spawn(command, arg);

    sp.stdout.on('data', function (data) 
    {
    	callback(data.toString());
    });

	sp.on('close', function (code) 
    {
        callback(code);
    });

    sp.on('error', function (code) 
    {
        console.log('asdsa');
    });
}

function CommandWait(command, arg, callback) 
{
    var sp = spawn(command, arg);
    var res = "";

    sp.stdout.on('data', function (data) 
    {
    	res += data.toString();
    });

	sp.on('close', function (code) 
    {
        callback(res);
    });
}


function Exec(command, callback)
{
    exec(command, function(error, stdout, stderr)
    {
        callback(stdout, error, stderr);
    });
}

function Connection()
{
    var connection = mysql.createConnection(
        {
            host        : process.env.DB_HOST,
            database    : process.env.DB_DATABASE,
            user        : process.env.DB_USERNAME,
            password    : process.env.DB_PASSWORD,
        });
    return connection;
}

function RegisterDomainName(io, socket, connection, json, callback)
{
	var date = new Date();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var year = date.getFullYear();


	var sql = "INSERT INTO domains (user_id, name,type, deleteEdit, created_at) VALUES (?,?,?,0, NOW())";
	var data = [1, json.DomainName, 'NATIVE'];
	sql = connection.format(sql, data);
    connection.query(sql, function(err, rows, fields)
    	{
    		socket.emit('buildServer-client', ' - Add ' + json.DomainName + ' as server domain name');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, json.DomainName, 'SOA', 'ns1.' + json.DomainName + ' root.ns1.' + json.DomainName + ' ' + year +  month + day + '01 86400 7200 604800 300', 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert SOA record');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, json.DomainName, 'NS', 'ns1.' + json.DomainName, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert NS1 record');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, json.DomainName, 'NS', 'ns2.' + json.DomainName, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert NS2 record');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, 'ns1.' + json.DomainName, 'A', process.env.DNS_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert A record for NS1');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, 'ns2.' + json.DomainName, 'A', process.env.DNS_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert A record for NS2');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, json.DomainName, 'A', process.env.MAIN_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert A record');
    	});


    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, json.ServerName, 'A', process.env.MAIN_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
        {
            socket.emit('buildServer-client', ' - Insert A record for server');
        });


    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, 'db.' + json.DomainName, 'A', process.env.DATABASE_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
        {
            socket.emit('buildServer-client', ' - Insert A record');
        });


    sql = "INSERT INTO records (domain_id,name,type,content,ttl,deleteEdit) VALUES (?,?,?,?,?,0)";
    data = [1, 'mail.' + json.DomainName, 'A', process.env.EMAIL_SERVER, 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
        {
            socket.emit('buildServer-client', ' - Insert A record');
        });


    sql = "INSERT INTO records (domain_id,name,type,content,ttl,prio,deleteEdit) VALUES (?,?,?,?,?,?,0)";
    data = [1, json.DomainName, 'MX', 'mail.' + json.DomainName, 600, 0];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert MX record');
    	});

    sql = "INSERT INTO records (domain_id,name,type,content,ttl) VALUES (?,?,?,?,?)";
    data = [1, json.DomainName, 'TXT', 'v=spf1 mx a -all', 86400];
    sql = connection.format(sql, data);
    connection.query(sql, function()
    	{
    		socket.emit('buildServer-client', ' - Insert TXT record');
    		callback();
    	});
}