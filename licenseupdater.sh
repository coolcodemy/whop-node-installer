#!/bin/bash

# $1 = license location


cd /tmp/
rm -rf license.tar.gz
rm -rf whop-license

cp -p $1 /tmp/
tar -zxvf license.tar.gz
cd whop-license
./update.sh
cd ../
rm -rf whop-license*